package com.curso.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.entity.Curso;
import com.curso.repository.CursoDAO;

@Service
public class CursoServiceImpl implements CursoService {

	@Autowired
	CursoDAO dao;

	@Override
	public List<Curso> dameCursos() {
		return dao.findAll();
	}

	@Override
	public Curso buscarCurso(Integer codCurso) {
		return dao.findById(codCurso).orElse(null);
	}

	@Override
	public void altaCurso(Curso curso) {
		dao.save(curso);
	}

	@Override
	public void actualizarCurso(Curso curso) {
		dao.save(curso);
	}

	@Override
	public void eliminarCurso(Integer codCurso) {
		dao.deleteById(codCurso);
	}

	@Override
	public Curso buscaCursoNombre(String nombre) {
		for (Curso curso : dameCursos()) {
			if (curso.getNombre().equals(nombre)) {
				return curso;
			}
		}
		return null;
	}

}
