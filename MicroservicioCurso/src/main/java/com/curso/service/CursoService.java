package com.curso.service;

import java.util.List;
import com.curso.entity.Curso;

public interface CursoService {

	List<Curso> dameCursos();
	Curso buscarCurso(Integer codCurso);
	void altaCurso(Curso curso);
	void actualizarCurso(Curso curso);
	void eliminarCurso(Integer codCurso);
	
	Curso buscaCursoNombre(String nombre);
}
