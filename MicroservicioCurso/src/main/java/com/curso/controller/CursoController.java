package com.curso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.entity.Curso;
import com.curso.service.CursoService;

@RestController
@RequestMapping("/cursos") // http://localhost:8080/cursos
public class CursoController {

	@Autowired
	CursoService service;

	@GetMapping
	public ResponseEntity<List<Curso>> listar() {
		List<Curso> cursos = service.dameCursos();
		if (cursos.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(cursos);
	}

	@GetMapping("/{codCurso}")
	public ResponseEntity<Curso> buscar(@PathVariable Integer codCurso) {
		return ResponseEntity.ok(service.buscarCurso(codCurso));
	}

	@PostMapping
	public void alta(@RequestBody Curso curso) {
		service.altaCurso(curso);
	}

	@PutMapping
	public void actualizar(@RequestBody Curso curso) {
		service.actualizarCurso(curso);
	}

	@DeleteMapping("/{codCurso}")
	public void eliminar(@PathVariable Integer codCurso) {
		service.eliminarCurso(codCurso);
	}
	
	@GetMapping("/{nombre}")
	public ResponseEntity<Curso> buscar(@PathVariable String nombre) {
		return ResponseEntity.ok(service.buscaCursoNombre(nombre));
	}
}
