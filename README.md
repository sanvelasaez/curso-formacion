# Nombre del Proyecto: Microservicios de Cursos y Formaciones

El proyecto consiste en dos microservicios interconectados: uno para la gestión de cursos como una API y otro para el control de dicha API. El microservicio de Cursos se conecta a una tabla en una base de datos MySQL utilizando Hibernate como ORM (Object-Relational Mapping), mientras que el microservicio de Formaciones se encarga de interactuar con el microservicio de Cursos para crear cursos y listarlos.

## Descripción del Proyecto

El proyecto se compone de los siguientes microservicios:

### Microservicio de Cursos

Este microservicio se encarga de gestionar los cursos como una API. Proporciona funcionalidades como:

- Consultar la lista de cursos disponibles.
- Crear un nuevo curso.
- Actualizar un curso existente.
- Eliminar un curso.

El microservicio de Cursos se conecta a una tabla en una base de datos MySQL utilizando Hibernate JPA para realizar operaciones de persistencia.

### Microservicio de Formaciones
Este microservicio actúa como gestor para controlar los cursos. Ofrece las siguientes funcionalidades:

- Consultar la lista de cursos disponibles.
- Crear un nuevo curso utilizando el microservicio de Cursos.

El microservicio de Formaciones se comunica con el microservicio de Cursos utilizando RESTful API y la tecnología Spring. Se utiliza RestTemplate para realizar solicitudes HTTP entre los microservicios.

## Tecnologías Utilizadas

El proyecto ha sido desarrollado utilizando las siguientes tecnologías y herramientas:

- Java: Lenguaje de programación utilizado para implementar la lógica de los microservicios.
- Spring: Framework de desarrollo de aplicaciones Java utilizado para construir aplicaciones empresariales.
- Hibernate JPA: Implementación de la especificación JPA (Java Persistence API) utilizada para el mapeo objeto-relacional y la interacción con la base de datos.
- Eclipse: Entorno de desarrollo integrado (IDE) utilizado para desarrollar y administrar el proyecto.
- MySQL: Sistema de gestión de bases de datos relacional utilizado para almacenar la información de los cursos.
- RestTemplate: Biblioteca de Spring utilizada para realizar solicitudes HTTP entre los microservicios.

## Configuración del Proyecto

Para configurar y ejecutar el proyecto en tu entorno local, sigue los pasos a continuación:

1. Clona este repositorio en tu máquina local o descárgalo como archivo ZIP.
2. Abre el proyecto en Eclipse o importa los microservicios en tu IDE preferido.
3. Asegúrate de tener un servidor MySQL instalado y configurado con una base de datos adecuada.
4. Configura las conexiones a la base de datos en los archivos de configuración correspondientes (por ejemplo, `application.properties`), proporcionando la URL de la base de datos, el nombre de usuario y la contraseña.
5. Compila y ejecuta cada microservicio por separado. Asegúrate de que no haya errores.
6. Los microservicios estarán disponibles en las URL correspondientes, por ejemplo, `http://localhost:8080/cursos` para el microservicio de Cursos y `http://localhost:8081/formaciones` para el microservicio de Formaciones.

## Contribución

Si deseas contribuir a este proyecto, puedes seguir los pasos a continuación:

1. Realiza un fork de este repositorio y clónalo en tu máquina local.
2. Crea una rama nueva para realizar tus modificaciones: `git checkout -b feature/nueva-caracteristica`.
3. Realiza las modificaciones y realiza confirmaciones (commits) claros para describir tus cambios.
4. Publica tus cambios en tu repositorio remoto: `git push origin feature/nueva-caracteristica`.
5. Crea una solicitud de extracción (pull request) en este repositorio y describe tus modificaciones detalladamente.

Todas las contribuciones son bienvenidas. Cualquier corrección de errores, mejoras de funcionalidad o nuevas características serán apreciadas.

## Contacto

Si tienes alguna pregunta, sugerencia o problema relacionado con el proyecto, no dudes en contactar al equipo de desarrollo a través del siguiente correo electrónico: [mi-correo-electronico@example.com](mailto:tu-correo-electronico@example.com).
