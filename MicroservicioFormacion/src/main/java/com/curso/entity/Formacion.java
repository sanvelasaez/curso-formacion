package com.curso.entity;

public class Formacion {

	private String nombre;
	private Integer numAsignaturas;
	private Double precio;

	/**
	 * @param nombre
	 * @param numAsignaturas
	 * @param precio
	 */
	public Formacion(String nombre, Integer numAsignaturas, Double precio) {
		this.nombre = nombre;
		this.numAsignaturas = numAsignaturas;
		this.precio = precio;
	}

	public Formacion() {
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the numAsignaturas
	 */
	public Integer getNumAsignaturas() {
		return numAsignaturas;
	}

	/**
	 * @param numAsignaturas the numAsignaturas to set
	 */
	public void setNumAsignaturas(Integer numAsignaturas) {
		this.numAsignaturas = numAsignaturas;
	}

	/**
	 * @return the precio
	 */
	public Double getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(Double precio) {
		this.precio = precio;
	}

}
