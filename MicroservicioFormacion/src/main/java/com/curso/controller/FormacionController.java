package com.curso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.entity.Formacion;
import com.curso.model.Curso;
import com.curso.service.FormacionService;

@RestController
@RequestMapping("/formaciones") // http://localhost:8081/formaciones
public class FormacionController {

	@Autowired
	FormacionService service;

	@GetMapping
	public ResponseEntity<List<Curso>> listar() {
		List<Curso> cursos = service.dameCursos();
		if (cursos.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(cursos);
	}

	@PostMapping
	public void alta(@RequestBody Formacion formacion) {
		service.altaFormacion(formacion);
	}
}
