package com.curso.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.curso.model.Curso;
import com.curso.entity.Formacion;

@Service
public class FormacionServiceImpl implements FormacionService {

	@Autowired
	RestTemplate template;

	String url = "http://localhost:8080/cursos/";

	private List<Formacion> formaciones = new ArrayList<>();

	public List<Formacion> dameFormaciones() {
		formaciones.add(new Formacion("java", 10, 500.0));
		return formaciones;
	}

	@Override
	public List<Curso> dameCursos() {
		return template.getForObject(url, List.class);
	}

	@Override
	public void altaFormacion(Formacion formacion) {
		// Comprobar si existe ya la formacion con el mismo nombre
		Curso curso = template.getForObject(url + formacion.getNombre(), Curso.class);
//		boolean q = true;
//		if (formacion.getNombre().equals(curso.getNombre()))
//			q = false;

		if (curso==null) {
			Curso cursoNuevo=new Curso();
			cursoNuevo.setNombre(formacion.getNombre());
			cursoNuevo.setDuracion(formacion.getNumAsignaturas()*10);
			template.postForObject(url, cursoNuevo, Curso.class);
		}
	}

}
