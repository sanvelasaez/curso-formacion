package com.curso.service;

import java.util.List;
import com.curso.entity.Formacion;
import com.curso.model.Curso;

public interface FormacionService {

	List<Curso> dameCursos();

	void altaFormacion(Formacion formacion);

}
