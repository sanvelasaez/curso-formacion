package com.curso.model;

public class Curso {
	
	private Integer codCurso;
	private String nombre;
	private Integer duracion;
	private Double precio;
	
	public Curso() {
	}
	/**
	 * @return the codCurso
	 */
	public Integer getCodCurso() {
		return codCurso;
	}
	/**
	 * @param codCurso the codCurso to set
	 */
	public void setCodCurso(Integer codCurso) {
		this.codCurso = codCurso;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the duracion
	 */
	public Integer getDuracion() {
		return duracion;
	}
	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}
	/**
	 * @return the precio
	 */
	public Double getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	
}
